﻿namespace Unittests.V2
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Application.FTP.V2;
    using Application.FTP.V2.DTO;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        private static Guid realTestID = new Guid("bbeb11f1-12e2-4b0b-91ec-98fe321a6867");
        private Logic logic = new Logic();

        private FTPSettings settings = new FTPSettings() { id = realTestID, userName = "MyStoryStar", password = "nZKMJ3", serverUri = "ftp://94.231.99.22:21/", enableSsl = true, usePassive = true, ignoreCertificate = true };

        /// <summary>
        /// Fileprint
        /// </summary>
        /// <param name="item">FTPFileList item</param>
        public void Fileprint(FTPFileList item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            foreach (FTPFileList f in item.items)
            {
                if (f.fileSize != "<DIR>")
                {
                    // (f.Directory + f.Name + "  - size " + f.FileSize + " byte  - date " + f.CreationDate);
                }
                else
                {
                    // (f.Directory + f.Name + "  -  " + f.FileSize + "  - date " + f.CreationDate);
                }

                if (f.items.Count > 0)
                {
                    this.Fileprint(f);
                }
            }
        }

        /// <summary>
        /// MakeNewSettingFile
        /// </summary>
        [Test]
        public void MakeNewSettingFile()
        {
            bool result = false;

            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            if (File.Exists($"\\FTPSettings\\" + realTestID))
            {
                result = true;
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Settings loaded");
        }

        /// <summary>
        /// LoadSettingTest
        /// </summary>
        [Test]
        public void LoadSettingTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            this.settings = new FTPSettings();

            this.settings = this.logic.LoadSettings(realTestID);

            // ("Loaded settings id {0} user {1} password {2} Uri {3}", settings.Id, settings.Username, settings.Password, settings.ServerUri);
            bool result;

            if (string.IsNullOrEmpty(this.settings.password))
            {
                result = false;
            }
            else
            {
                result = true;
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Settings loaded");
        }

        /// <summary>
        /// SaveUpdateSettingPasswordTest
        /// </summary>
        [Test]
        public void SaveUpdateSettingPasswordTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            this.settings = new FTPSettings();
            this.settings = this.logic.LoadSettings(realTestID);

            // ("Pasword before = " + settings.Password);
            this.settings.password = "4321";

            this.logic.SaveSettings(this.settings);

            this.settings = new FTPSettings();
            this.settings = this.logic.LoadSettings(realTestID);

            // ("Pasword after = " + settings.Password);
            bool result;

            if (this.settings.password == "4321")
            {
                result = true;
            }
            else
            {
                result = false;
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "1234 password was saved");
        }

        /// <summary>
        /// FTPFileListTest
        /// </summary>
        [Test]
        public void FTPFileListTest()
        {
            bool result = false;

            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testfilePath = @".\v2\TestItems\TestUpload.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testfilePath);
            testfile.fileContent = File.ReadAllBytes(testfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = string.Empty;
            this.logic.UploadFiles(uploadRequest);

            FTPFileListRequest request = new FTPFileListRequest();
            request.ftpId = realTestID;
            request.directory = string.Empty;

            FTPFileList files = this.logic.ListFiles(request);

            foreach (FTPFileList file in files.items)
            {
                if (file.name == "TestUpload.txt")
                {
                    result = true;
                }
            }

            FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.fileInfo = new List<FTPInfo>();
            FTPInfo file1 = new FTPInfo();
            file1.directory = string.Empty;
            file1.name = "TestUpload.txt";
            removeRequest.fileInfo.Add(file1);
            this.logic.RemoveFiles(removeRequest);

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Files printed");
        }

        /// <summary>
        /// FTPFileListInFolderTest
        /// </summary>
        [Test]
        public void FTPFileListInFolderTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPFolderRequest makeFolderRequest = new FTPFolderRequest();
            makeFolderRequest.ftpId = realTestID;
            makeFolderRequest.folderInfo = new List<FTPInfo>();
            FTPInfo folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            makeFolderRequest.folderInfo.Add(folder1);
            this.logic.AddFolders(makeFolderRequest);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testfilePath = @".\v2\TestItems\TestUpload.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testfilePath);
            testfile.fileContent = File.ReadAllBytes(testfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = "TestFolder/";
            this.logic.UploadFiles(uploadRequest);

            FTPFileListRequest request = new FTPFileListRequest();
            request.ftpId = realTestID;
            request.directory = "TestFolder/";
            bool result = false;

            FTPFileList files = this.logic.ListFiles(request);

            foreach (FTPFileList file in files.items)
            {
                if (file.name == "TestUpload.txt")
                {
                    result = true;
                }
            }

            FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.fileInfo = new List<FTPInfo>();
            FTPInfo file1 = new FTPInfo();
            file1.directory = "TestFolder/";
            file1.name = "TestUpload.txt";
            removeRequest.fileInfo.Add(file1);
            this.logic.RemoveFiles(removeRequest);

            FTPFolderRequest removeFolderRequest = new FTPFolderRequest();
            removeFolderRequest.ftpId = realTestID;
            removeFolderRequest.folderInfo = new List<FTPInfo>();
            folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            removeFolderRequest.folderInfo.Add(folder1);
            this.logic.RemoveFolders(removeFolderRequest);

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Files printed");
        }

        /// <summary>
        /// FTPUploadOneFileTest
        /// </summary>
        [Test]
        public void FTPUploadOneFileTest()
        {
            var result = false;

            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPUploadRequest request = new FTPUploadRequest();

            string testfilePath = @".\v2\TestItems\TestUpload.txt";

            if (File.Exists(testfilePath))
            {
                request.files = new List<FileData>();

                FileData testfile = new FileData();
                testfile.fileName = Path.GetFileName(testfilePath);
                testfile.fileContent = File.ReadAllBytes(testfilePath).ToList();

                request.files.Add(testfile);
                request.fTPId = realTestID;
                request.uploadToFolder = string.Empty;

                List<FTPOpResult> res = this.logic.UploadFiles(request);

                int nr = 0;
                foreach (FTPOpResult r in res)
                {
                    // ("status file {0} disc {1} code {2}", nr, r.description, r.code);
                    nr++;
                }

                // Checker om filen er på ftp serveren
                FTPFileListRequest fileRequest = new FTPFileListRequest();
                fileRequest.ftpId = request.fTPId;
                fileRequest.directory = request.uploadToFolder;

                FTPFileList files = this.logic.ListFiles(fileRequest);

                if (files.items.Count > 0)
                {
                    foreach (FTPFileList f in files.items)
                    {
                        if (testfile.fileName == f.name)
                        {
                            result = true;
                        }
                    }
                }

                FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
                removeRequest.ftpId = realTestID;
                removeRequest.fileInfo = new List<FTPInfo>();
                FTPInfo file1 = new FTPInfo();
                file1.directory = string.Empty;
                file1.name = "TestUpload.txt";
                removeRequest.fileInfo.Add(file1);
                this.logic.RemoveFiles(removeRequest);
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Files uploaded");
        }

        /// <summary>
        /// FTPUploadOneFileToFolderTest
        /// </summary>
        [Test]
        public void FTPUploadOneFileToFolderTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPFolderRequest makeFolderRequest = new FTPFolderRequest();
            makeFolderRequest.ftpId = realTestID;
            makeFolderRequest.folderInfo = new List<FTPInfo>();
            FTPInfo folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            makeFolderRequest.folderInfo.Add(folder1);
            this.logic.AddFolders(makeFolderRequest);

            FTPUploadRequest request = new FTPUploadRequest();
            var result = false;
            string testfilePath = @".\v2\TestItems\TestDoc.txt";

            if (File.Exists(testfilePath))
            {
                request.files = new List<FileData>();

                FileData testfile = new FileData();
                testfile.fileName = Path.GetFileName(testfilePath);
                testfile.fileContent = File.ReadAllBytes(testfilePath).ToList();

                request.files.Add(testfile);
                request.fTPId = realTestID;
                request.uploadToFolder = "TestFolder/";

                List<FTPOpResult> res = this.logic.UploadFiles(request);

                int nr = 0;
                foreach (FTPOpResult r in res)
                {
                    // ("status file {0} disc {1} code {2}", nr, r.description, r.code);
                    nr++;
                }

                // Checker om filen er på ftp serveren
                FTPFileListRequest fileRequest = new FTPFileListRequest();
                fileRequest.ftpId = request.fTPId;
                fileRequest.directory = request.uploadToFolder;

                FTPFileList files = this.logic.ListFiles(fileRequest);

                if (files.items.Count > 0)
                {
                    foreach (FTPFileList f in files.items)
                    {
                        if (testfile.fileName == f.name)
                        {
                            result = true;
                        }
                    }
                }
            }

            FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.fileInfo = new List<FTPInfo>();
            FTPInfo file1 = new FTPInfo();
            file1.directory = "TestFolder/";
            file1.name = "TestDoc.txt";
            removeRequest.fileInfo.Add(file1);
            this.logic.RemoveFiles(removeRequest);

            FTPFolderRequest removeFolderRequest = new FTPFolderRequest();
            removeFolderRequest.ftpId = realTestID;
            removeFolderRequest.folderInfo = new List<FTPInfo>();
            folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            removeFolderRequest.folderInfo.Add(folder1);
            this.logic.RemoveFolders(removeFolderRequest);

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "Files uploaded");
        }

        /// <summary>
        /// FTPFileDownloadTest
        /// </summary>
        [Test]
        public void FTPFileDownloadTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testUploadfilePath = @".\v2\TestItems\TestUpload.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testUploadfilePath);
            testfile.fileContent = File.ReadAllBytes(testUploadfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = string.Empty;
            this.logic.UploadFiles(uploadRequest);

            var result = false;

            FTPDownloadFileRequest request = new FTPDownloadFileRequest();

            request.ftpId = realTestID;
            request.requestInfo = new List<FTPInfo>();
            FTPInfo info2 = new FTPInfo();
            info2.directory = string.Empty;
            info2.name = "TestUpload.txt";
            request.requestInfo.Add(info2);

            request.deleteAfterDownload = false;

            FTPDownloadData files = this.logic.DownloadFile(request);

            foreach (FTPFileData file in files.files)
            {
                string testfilePath = @".\v2\TestToFolder\";
                string saveFilePath = testfilePath + file.fileMetadata.fileName;

                File.WriteAllBytes(saveFilePath, file.fileContent.ToArray());

                // (file.FileMetadata.Directory + file.FileMetadata.Filename + "  - size " + file.FileMetadata.FileSize + " byte  - date " + file.FileMetadata.CreationDate);
                if (File.Exists(saveFilePath))
                {
                    result = true;
                }
            }

            FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.fileInfo = new List<FTPInfo>();
            FTPInfo file1 = new FTPInfo();
            file1.directory = string.Empty;
            file1.name = "TestUpload.txt";
            removeRequest.fileInfo.Add(file1);
            this.logic.RemoveFiles(removeRequest);

            File.Delete(@".\v2\TestToFolder\" + files.files[0].fileMetadata.fileName);
            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "File downloaded");
        }

        /// <summary>
        /// FTPFileDownloadAndRemoveTest
        /// </summary>
        [Test]
        public void FTPFileDownloadAndRemoveTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testUploadfilePath = @".\v2\TestItems\TestDoc.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testUploadfilePath);
            testfile.fileContent = File.ReadAllBytes(testUploadfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = string.Empty;
            this.logic.UploadFiles(uploadRequest);

            var result = false;

            FTPDownloadFileRequest request = new FTPDownloadFileRequest();

            request.ftpId = realTestID;
            request.requestInfo = new List<FTPInfo>();
            FTPInfo info1 = new FTPInfo();
            info1.directory = string.Empty;
            info1.name = "TestDoc.txt";
            request.requestInfo.Add(info1);
            request.deleteAfterDownload = true;

            FTPDownloadData files = this.logic.DownloadFile(request);

            foreach (FTPFileData file in files.files)
            {
                string testfilePath = @".\v2\TestToFolder\";
                string saveFilePath = testfilePath + file.fileMetadata.fileName;

                File.WriteAllBytes(saveFilePath, file.fileContent.ToArray());

                // (file.FileMetadata.Directory + file.FileMetadata.Filename + "  - size " + file.FileMetadata.FileSize + " byte  - date " + file.FileMetadata.CreationDate);
                if (File.Exists(saveFilePath))
                {
                    result = true;
                }
            }

            File.Delete(@".\v2\TestToFolder\" + files.files[0].fileMetadata.fileName);
            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "File downloaded and removed");
        }

        /// <summary>
        /// FTPFileDownloadFromFolderTest
        /// </summary>
        [Test]
        public void FTPFileDownloadFromFolderTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPFolderRequest makeFolderRequest = new FTPFolderRequest();
            makeFolderRequest.ftpId = realTestID;
            makeFolderRequest.folderInfo = new List<FTPInfo>();
            FTPInfo folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            makeFolderRequest.folderInfo.Add(folder1);
            this.logic.AddFolders(makeFolderRequest);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testUploadfilePath = @".\v2\TestItems\TestDoc.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testUploadfilePath);
            testfile.fileContent = File.ReadAllBytes(testUploadfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = "TestFolder/";
            this.logic.UploadFiles(uploadRequest);

            var result = false;
            FTPDownloadFileRequest request = new FTPDownloadFileRequest();

            request.ftpId = realTestID;
            request.requestInfo = new List<FTPInfo>();
            FTPInfo info1 = new FTPInfo();
            info1.directory = "TestFolder/";
            info1.name = "TestDoc.txt";
            request.requestInfo.Add(info1);
            request.deleteAfterDownload = false;

            FTPDownloadData files = this.logic.DownloadFile(request);

            foreach (FTPFileData file in files.files)
            {
                string testfilePath = @".\v2\TestToFolder\";
                string saveFilePath = testfilePath + file.fileMetadata.fileName;

                File.WriteAllBytes(saveFilePath, file.fileContent.ToArray());

                // (file.FileMetadata.Directory + file.FileMetadata.Filename + "  - size " + file.FileMetadata.FileSize + " byte  - date " + file.FileMetadata.CreationDate);
                if (File.Exists(saveFilePath))
                {
                    result = true;
                }
            }

            FTPFileRemoveRequest removeRequest = new FTPFileRemoveRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.fileInfo = new List<FTPInfo>();
            FTPInfo file1 = new FTPInfo();
            file1.directory = "TestFolder/";
            file1.name = "TestDoc.txt";
            removeRequest.fileInfo.Add(file1);
            this.logic.RemoveFiles(removeRequest);

            FTPFolderRequest removeFolderRequest = new FTPFolderRequest();
            removeFolderRequest.ftpId = realTestID;
            removeFolderRequest.folderInfo = new List<FTPInfo>();
            folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            removeFolderRequest.folderInfo.Add(folder1);
            this.logic.RemoveFolders(removeFolderRequest);

            File.Delete(@".\v2\TestToFolder\" + files.files[0].fileMetadata.fileName);
            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "File downloaded");
        }

        /// <summary>
        /// FTPFileDeletTest
        /// </summary>
        [Test]
        public void FTPFileDeletTest()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPUploadRequest uploadRequest = new FTPUploadRequest();
            string testfilePath = @".\v2\TestItems\TestUpload.txt";
            uploadRequest.files = new List<FileData>();
            FileData testfile = new FileData();
            testfile.fileName = Path.GetFileName(testfilePath);
            testfile.fileContent = File.ReadAllBytes(testfilePath).ToList();
            uploadRequest.files.Add(testfile);
            uploadRequest.fTPId = realTestID;
            uploadRequest.uploadToFolder = string.Empty;
            this.logic.UploadFiles(uploadRequest);

            var result = true;

            FTPFileRemoveRequest request = new FTPFileRemoveRequest();

            request.ftpId = realTestID;
            request.fileInfo = new List<FTPInfo>();

            FTPInfo file1 = new FTPInfo();
            file1.directory = string.Empty;
            file1.name = "TestUpload.txt";
            request.fileInfo.Add(file1);

            FTPStatus @out = this.logic.RemoveFiles(request);

            foreach (string outInfo in @out.info)
            {
                // Console.WriteLine(OutInfo);
            }

            // Checker om filen er fjernet fra ftp serveren
            FTPFileListRequest fileRequest = new FTPFileListRequest();
            fileRequest.ftpId = request.ftpId;

            foreach (FTPInfo testFile in request.fileInfo)
            {
                fileRequest.directory = testFile.directory;

                FTPFileList files = this.logic.ListFiles(fileRequest);

                if (files.items.Count > 0)
                {
                    foreach (FTPFileList f in files.items)
                    {
                        if (testFile.name == f.name)
                        {
                            result = false;
                        }
                    }
                }
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "File deleted");
        }

        /// <summary>
        /// FTPMakeFolder
        /// </summary>
        [Test]
        public void FTPMakeFolder()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            var result = false;

            FTPFolderRequest request = new FTPFolderRequest();

            request.ftpId = realTestID;
            request.folderInfo = new List<FTPInfo>();

            FTPInfo folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            request.folderInfo.Add(folder1);

            FTPInfo folder2 = new FTPInfo();
            folder2.directory = string.Empty;
            folder2.name = "TestFolder2";
            request.folderInfo.Add(folder2);

            FTPStatus @out = this.logic.AddFolders(request);

            foreach (string outInfo in @out.info)
            {
                // Console.WriteLine(OutInfo);
            }

            // Checker om filen er fjernet fra ftp serveren
            FTPFileListRequest fileRequest = new FTPFileListRequest();
            fileRequest.ftpId = request.ftpId;

            foreach (FTPInfo testFolder in request.folderInfo)
            {
                fileRequest.directory = testFolder.directory;

                FTPFileList files = this.logic.ListFiles(fileRequest);

                if (files.items.Count > 0)
                {
                    foreach (FTPFileList f in files.items)
                    {
                        if (testFolder.name == f.name)
                        {
                            result = true;
                        }
                    }
                }
            }

            FTPFolderRequest removeRequest = new FTPFolderRequest();
            removeRequest.ftpId = realTestID;
            removeRequest.folderInfo = new List<FTPInfo>();
            removeRequest.folderInfo.Add(folder1);
            removeRequest.folderInfo.Add(folder2);
            this.logic.RemoveFolders(removeRequest);

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "True should be true :-)");
        }

        /// <summary>
        /// FTPRemoveFolder
        /// </summary>
        [Test]
        public void FTPRemoveFolder()
        {
            if (!Directory.Exists($"\\FTPSettings\\"))
            {
                Directory.CreateDirectory($"\\FTPSettings\\");
            }

            this.logic.SaveSettings(this.settings);

            FTPFolderRequest addRequest = new FTPFolderRequest();
            addRequest.ftpId = realTestID;
            addRequest.folderInfo = new List<FTPInfo>();
            FTPInfo folder1 = new FTPInfo();
            folder1.directory = string.Empty;
            folder1.name = "TestFolder";
            addRequest.folderInfo.Add(folder1);
            FTPInfo folder2 = new FTPInfo();
            folder2.directory = string.Empty;
            folder2.name = "TestFolder2";
            addRequest.folderInfo.Add(folder2);
            this.logic.AddFolders(addRequest);

            var result = true;

            FTPFolderRequest request = new FTPFolderRequest();

            request.ftpId = realTestID;
            request.folderInfo = new List<FTPInfo>();

            request.folderInfo.Add(folder1);

            request.folderInfo.Add(folder2);

            FTPStatus @out = this.logic.RemoveFolders(request);

            foreach (string outInfo in @out.info)
            {
                // Console.WriteLine(OutInfo);
            }

            // Checker om filen er fjernet fra ftp serveren
            FTPFileListRequest fileRequest = new FTPFileListRequest();
            fileRequest.ftpId = request.ftpId;

            foreach (FTPInfo testFolder in request.folderInfo)
            {
                fileRequest.directory = testFolder.directory;

                FTPFileList files = this.logic.ListFiles(fileRequest);

                if (files.items.Count > 0)
                {
                    foreach (FTPFileList f in files.items)
                    {
                        if (testFolder.name == f.name)
                        {
                            result = false;
                        }
                    }
                }
            }

            File.Delete($"\\FTPSettings\\" + realTestID);

            Assert.IsTrue(result, "True should be true :-)");
        }
    }
}
