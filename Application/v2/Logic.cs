﻿namespace Application.FTP.V2
{
    // NOTE : This microservice uses CM.SMS, which is .NET FULL/CLI - dont expect this to run on linux!
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using Application.FTP.V2.DTO;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;

    /// <summary>
    /// Logic.
    /// </summary>
    public class Logic
    {
        private DateTime MakeDateTime(string dayIn, string timeIn, bool isWindows)
        {
            string[] dateinfo = dayIn.Split('-');
            int[] date = new int[dateinfo.Length];
            DateTime dayOut;

            if (isWindows)
            {
                int.TryParse(dateinfo[0], out date[0]);
                date[1] = int.Parse(dateinfo[1], ITUtil.Common.Constants.Culture.DefaultCulture);
                date[2] = int.Parse(dateinfo[2], ITUtil.Common.Constants.Culture.DefaultCulture);
                if (date[2] < 2000)
                {
                    date[2] += 2000;
                }

                string[] timeinfo = timeIn.Split(':');
                int[] time = new int[timeinfo.Length];
                time[0] = Convert.ToInt32(timeinfo[0], ITUtil.Common.Constants.Culture.DefaultCulture);

                if (timeinfo[1].Contains('A', StringComparison.OrdinalIgnoreCase))
                {
                    time[1] = int.Parse(timeinfo[1].Split('A')[0], ITUtil.Common.Constants.Culture.DefaultCulture);
                }
                else
                {
                    time[1] = int.Parse(timeinfo[1].Split('P')[0], ITUtil.Common.Constants.Culture.DefaultCulture);
                    time[0] += 12;
                }

                dayOut = new DateTime(date[2], date[0], date[1], time[0], time[1], 00);
            }
            else
            {
                dayOut = DateTime.Parse(dateinfo[1] + ". " + dateinfo[0] + " " + dateinfo[2], ITUtil.Common.Constants.Culture.DefaultCulture);
            }

            return dayOut;
        }

        private List<string> ParsFileInfo(string line, bool isWindows)
        {
            List<string> info = new List<string>();

            if (isWindows)
            {
                string[] splitInfo = line.Split(' '); // 0 = Date  1 = Time  2 = type  3 = name

                foreach (string s in splitInfo)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        info.Add(s);
                    }
                }
            }
            else
            {
                string[] splitInfo = line.Split(' '); // 0 = Date  1 = Time  2 = type  3 = name
                List<string> temp = new List<string>();

                foreach (string s in splitInfo)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        temp.Add(s);
                    }
                }

                info.Add(temp[temp.Count - 4] + "-" + temp[temp.Count - 3] + "-" + temp[temp.Count - 2]);
                info.Add(string.Empty);
                if (temp[temp.Count - 5] == "0")
                {
                    info.Add("<DIR>");
                }
                else
                {
                    info.Add(temp[temp.Count - 5]);
                }

                info.Add(temp[temp.Count - 1]);
            }

            return info;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
        }

        /// <summary>
        /// LoadSettings
        /// </summary>
        /// <param name="id">Guid id</param>
        /// <returns>FTPSettings</returns>
        public FTPSettings LoadSettings(Guid id)
        {
            var settings = System.IO.File.ReadAllText($"\\FTPSettings\\{id}"); // checker ikke for om filen findes, da jeg gerne vil have en exception hvis den ikke gør...
            return Newtonsoft.Json.JsonConvert.DeserializeObject<FTPSettings>(settings);
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        /// <param name="newsettings">FTPSettings newsettings</param>
        public void SaveSettings(FTPSettings newsettings)
        {
            if (newsettings == null)
            {
                throw new ArgumentNullException(nameof(newsettings));
            }

            var settings = Newtonsoft.Json.JsonConvert.SerializeObject(newsettings);
            System.IO.File.WriteAllText($"\\FTPSettings\\{newsettings.id}", settings);
        }

        /// <summary>
        /// ListFiles
        /// </summary>
        /// <param name="request">FTPFileListRequest request</param>
        /// <returns>FTPFileList</returns>
        public FTPFileList ListFiles(FTPFileListRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            FTPSettings settings = this.LoadSettings(request.ftpId);
            FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + request.directory);
            ftprequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
            ftprequest.UsePassive = settings.usePassive;
            ftprequest.EnableSsl = settings.enableSsl;

            if (settings.ignoreCertificate)
            {
                ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
            }

            FTPFileList list = new FTPFileList();

            using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string line = reader.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {
                        bool isWindows = int.TryParse(line.Split('-')[0], out int test);
                        List<string> info = this.ParsFileInfo(line, isWindows);

                        FTPFileList item = new FTPFileList();

                        item.creationDate = this.MakeDateTime(info[0], info[1], isWindows);
                        item.fileSize = info[2];
                        item.name = info[3];
                        item.directory = request.directory;
                        list.items.Add(item);
                        line = reader.ReadLine();
                    }
                }
            }

            foreach (FTPFileList item in list.items)
            {
                if (!item.name.Contains('.', StringComparison.OrdinalIgnoreCase))
                {
                    FTPFileListRequest req = new FTPFileListRequest();
                    req.ftpId = request.ftpId;
                    req.directory = item.directory + item.name + "/";
                    item.items.AddRange(this.ListFiles(req).items);
                }
            }

            return list;
        }

        /// <summary>
        /// UploadFiles
        /// </summary>
        /// <param name="request">FTPUploadRequest request</param>
        /// <returns>List FTPOpResult</returns>
        public List<FTPOpResult> UploadFiles(FTPUploadRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            List<FTPOpResult> responese = new List<FTPOpResult>();

            foreach (var file in request.files)
            {
                // ServicePointManager.
                FTPSettings settings = this.LoadSettings(request.fTPId);
                FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + request.uploadToFolder + file.fileName);
                ftprequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
                ftprequest.UsePassive = settings.usePassive;
                ftprequest.EnableSsl = settings.enableSsl;

                ftprequest.ContentLength = file.fileContent.ToArray().Length;

                if (settings.ignoreCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                }

                using (Stream requestStream = ftprequest.GetRequestStream())
                {
                    requestStream.Write(file.fileContent.ToArray(), 0, file.fileContent.ToArray().Length);
                }

                using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                {
                    responese.Add(new FTPOpResult() { code = response.StatusCode, description = response.StatusDescription });

                    // onEvent("fileuploaded", new FTPOpResult() { code = response.StatusCode, description = response.StatusDescription }); //burde laves så man kan se hvor langt upload er
                }
            }

            return responese;
        }

        /// <summary>
        /// DownloadFile
        /// </summary>
        /// <param name="request">FTPDownloadFileRequest request</param>
        /// <returns>FTPDownloadData</returns>
        public FTPDownloadData DownloadFile(FTPDownloadFileRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            FTPDownloadData files = new FTPDownloadData();
            files.files = new List<FTPFileData>();

            foreach (FTPInfo info in request.requestInfo)
            {
                FTPSettings settings = this.LoadSettings(request.ftpId);
                FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + info.directory + info.name);
                ftprequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
                ftprequest.UsePassive = settings.usePassive;
                ftprequest.EnableSsl = settings.enableSsl;

                FTPFileData file = new FTPFileData();

                if (settings.ignoreCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                }

                using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    using (var memstream = new MemoryStream())
                    {
                        reader.BaseStream.CopyTo(memstream);
                        file.fileContent = memstream.ToArray().ToList();
                    }
                }

                FTPFileListRequest fileReq = new FTPFileListRequest();
                fileReq.directory = info.directory;
                fileReq.ftpId = request.ftpId;
                var filesInfo = this.ListFiles(fileReq);

                foreach (FTPFileList fileinfo in filesInfo.items)
                {
                    if (fileinfo.name == info.name)
                    {
                        file.fileMetadata = new FileMetadata();
                        file.fileMetadata.fileName = fileinfo.name;
                        file.fileMetadata.directory = fileinfo.directory;
                        file.fileMetadata.fileSize = fileinfo.fileSize;
                        file.fileMetadata.creationDate = fileinfo.creationDate;
                    }
                }

                if (request.deleteAfterDownload)
                {
                    FTPFileRemoveRequest removeReq = new FTPFileRemoveRequest();
                    removeReq.fileInfo = new List<FTPInfo>();
                    FTPInfo ftpinfo = new FTPInfo();
                    removeReq.fileInfo.Add(info);
                    removeReq.ftpId = request.ftpId;
                    this.RemoveFiles(removeReq);
                }

                files.files.Add(file);
            }

            return files;
        }

        /// <summary>
        /// RemoveFiles
        /// </summary>
        /// <param name="request">FTPFileRemoveRequest request</param>
        /// <returns>FTPStatus</returns>
        public FTPStatus RemoveFiles(FTPFileRemoveRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            FTPSettings settings = this.LoadSettings(request.ftpId);
            FTPStatus result = new FTPStatus();
            result.info = new List<string>();

            foreach (FTPInfo info in request.fileInfo)
            {
                FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + info.directory + info.name);
                ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
                ftprequest.UsePassive = settings.usePassive;
                ftprequest.EnableSsl = settings.enableSsl;

                if (settings.ignoreCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                }

                using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                {
                    result.info.Add("Delete state = " + response.StatusDescription);
                }
            }

            return result;
        }

        /// <summary>
        /// AddFolders
        /// </summary>
        /// <param name="request">FTPFolderRequest request</param>
        /// <returns>FTPStatus</returns>
        public FTPStatus AddFolders(FTPFolderRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            FTPSettings settings = this.LoadSettings(request.ftpId);
            FTPStatus result = new FTPStatus();
            result.info = new List<string>();

            foreach (FTPInfo info in request.folderInfo)
            {
                FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + info.directory + info.name);
                ftprequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
                ftprequest.UsePassive = settings.usePassive;
                ftprequest.EnableSsl = settings.enableSsl;

                if (settings.ignoreCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                }

                using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                {
                    result.info.Add("Folder state = " + response.StatusDescription);
                }
            }

            return result;
        }

        /// <summary>
        /// RemoveFolders
        /// </summary>
        /// <param name="request">FTPFolderRequest request</param>
        /// <returns>FTPStatus</returns>
        public FTPStatus RemoveFolders(FTPFolderRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            FTPSettings settings = this.LoadSettings(request.ftpId);
            FTPStatus result = new FTPStatus();
            result.info = new List<string>();

            foreach (FTPInfo info in request.folderInfo)
            {
                FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri + info.directory + info.name);
                ftprequest.Method = WebRequestMethods.Ftp.RemoveDirectory;
                ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
                ftprequest.UsePassive = settings.usePassive;
                ftprequest.EnableSsl = settings.enableSsl;

                if (settings.ignoreCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                }

                using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                {
                    result.info.Add("Folder state = " + response.StatusDescription);
                }
            }

            return result;
        }
    }
}
