﻿// <copyright file="FTPDownloadFileRequest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Application.FTP.V2.DTO
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    /// <summary>
    /// Combined object for login to get Token, User and Organisations in one request
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class FTPDownloadFileRequest
    {
        /// <summary>
        /// Gets or sets ftpId
        /// </summary>
        public Guid ftpId { get; set; }

        /// <summary>
        /// Gets or sets requestInfo
        /// </summary>
        public List<FTPInfo> requestInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether deleteAfterDownload
        /// </summary>
        public bool deleteAfterDownload { get; set; }
    }
}
