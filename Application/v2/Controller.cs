﻿namespace Application.FTP.V2
{
    // NOTE : This microservice uses CM.SMS, which is .NET FULL/CLI - dont expect this to run on linux!
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using Application.FTP.V2.DTO;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        private static Logic logic = new Logic();

        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "FTP";
            }
        }

        /// <summary>
        /// Admin
        /// </summary>
        public static MetaDataDefinition Admin = new MetaDataDefinition("FTP", "admin", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("EN", "FTP Admin"));

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "loadSettings",
                       "Loads settings and send to user",
                       typeof(FTPLoadSettingRequest),
                       typeof(FTPSettings),
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationLoadSettings),
                   new OperationDescription(
                       "updateSettings",
                       "Updates setting for FTP",
                       typeof(FTPSettings),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationSaveSettings),
                   new OperationDescription(
                       "listFiles",
                       "Lists all files on the FTP server",
                       typeof(FTPFileListRequest),
                       typeof(FTPFileList),
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationListFiles),
                   new OperationDescription(
                       "upload",
                       "Uploads files to the FTP server",
                       typeof(FTPUploadRequest),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationUpload),
                   new OperationDescription(
                       "download",
                       "Downloads a selected file from the FTP server, with possibility to delete after download",
                       typeof(FTPDownloadFileRequest),
                       typeof(FTPFileData),
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationDownloadFile),
                   new OperationDescription(
                       "removeFile",
                       "Removes a selected file from the FTP server",
                       typeof(FTPFileRemoveRequest),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationRemoveFile),
                   new OperationDescription(
                       "addFolder",
                       "Creates a new folder on the FTP server",
                       typeof(FTPFolderRequest),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationAddFolder),
                   new OperationDescription(
                       "removeFolder",
                       "Removes a folder on the FTP server",
                       typeof(FTPFolderRequest),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { Admin }),
                       ExecuteOperationRemoveFolder),
                };
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("2.0.0.0");
            }
        }

        /// <summary>
        /// ExecuteOperationLoadSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationLoadSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var settingsRequest = MessageDataHelper.FromMessageData<Guid>(wrapper.messageData);
            var settings = logic.LoadSettings(settingsRequest);
            return ReturnMessageWrapper.CreateResult(true, wrapper, settings, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationSaveSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSaveSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var newsettings = MessageDataHelper.FromMessageData<FTPSettings>(wrapper.messageData);
            logic.SaveSettings(newsettings);
            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationListFiles
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationListFiles(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var input = MessageDataHelper.FromMessageData<FTPFileListRequest>(wrapper.messageData);
            var result = logic.ListFiles(input);
            if (result != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, result, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "No files on FTP"));
            }
        }

        /// <summary>
        /// ExecuteOperationUpload
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationUpload(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<FTPUploadRequest>(wrapper.messageData);
            logic.UploadFiles(request);
            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationDownloadFile
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationDownloadFile(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<FTPDownloadFileRequest>(wrapper.messageData);
            var result = logic.DownloadFile(request);
            if (result != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, result, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "File does not exist"));
            }
        }

        /// <summary>
        /// ExecuteOperationAddFolder
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationAddFolder(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<FTPFolderRequest>(wrapper.messageData);
            var result = logic.AddFolders(request);
            if (result != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, result, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "File does not exist"));
            }
        }

        /// <summary>
        /// ExecuteOperationRemoveFolder
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationRemoveFolder(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<FTPFolderRequest>(wrapper.messageData);
            var result = logic.RemoveFolders(request);
            if (result != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, result, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Folder does not exist"));
            }
        }

        /// <summary>
        /// ExecuteOperationRemoveFile
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationRemoveFile(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<FTPFileRemoveRequest>(wrapper.messageData);
            var result = logic.RemoveFiles(request);
            if (result != null)
            {
                return ReturnMessageWrapper.CreateResult(true, wrapper, result, LocalizedString.OK);
            }
            else
            {
                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "File does not exist"));
            }
        }
    }
}
