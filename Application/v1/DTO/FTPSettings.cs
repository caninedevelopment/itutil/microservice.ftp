﻿// <copyright file="FTPSettings.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using System;
using System.Collections.Generic;
using System.Net;

namespace ITUtil.FTP.DTO
{
    /// <summary>
    /// FTPSettings
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class FTPSettings
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public Guid id { get; set; }

        /// <summary>
        /// Gets or sets serverUri
        /// </summary>
        public string serverUri { get; set; }

        /// <summary>
        /// Gets or sets userName
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// Gets or sets password
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether usePassive
        /// </summary>
        public bool usePassive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether enableSsl
        /// </summary>
        public bool enableSsl { get; set; }

        /// <summary>
        /// LoadSettings
        /// </summary>
        /// <param name="id">Guid id</param>
        /// <returns>FTPSettings</returns>
        public static FTPSettings LoadSettings(Guid id)
        {
            var settings = System.IO.File.ReadAllText($"\\FTPSettings\\{id}"); // checker ikke for om filen findes, da jeg gerne vil have en exception hvis den ikke gør...
            return Newtonsoft.Json.JsonConvert.DeserializeObject<FTPSettings>(settings);
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        /// <param name="newsettings">FTPSettings newsettings</param>
        public static void SaveSettings(FTPSettings newsettings)
        {
            if (newsettings == null)
            {
                throw new ArgumentNullException(nameof(newsettings));
            }

            var settings = Newtonsoft.Json.JsonConvert.SerializeObject(newsettings);
            System.IO.File.WriteAllText($"\\FTPSettings\\{newsettings.id}", settings);
        }
    }
}
