﻿namespace ITUtil.Microservice
{
    using System;

    // NOTE : This microservice uses CM.SMS, which is .NET FULL/CLI - dont expect this to run on linux!
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using ITUtil.FTP.DTO;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "FTP";
            }
        }

        /// <summary>
        /// Gets RouteNamespaces
        /// </summary>
        public string RouteNamespaces
        {
            get
            {
                return "FTP";
            }
        }

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "updateSettings",
                       "Updates the settings for FTP",
                       typeof(ITUtil.FTP.DTO.FTPSettings),
                       null,
                       null,
                       ExecuteOperationSettings),
                   new OperationDescription(
                       "upload",
                       "Uploads a file to FTP",
                       typeof(ITUtil.FTP.DTO.FTPUploadRequest),
                       null,
                       null,
                       ExecuteOperationUpload),
                };
            }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        /// <summary>
        /// ExecuteOperationSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var newsettings = MessageDataHelper.FromMessageData<ITUtil.FTP.DTO.FTPSettings>(wrapper.messageData);
            ITUtil.FTP.DTO.FTPSettings.SaveSettings(newsettings);
            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationUpload
        /// </summary>
        /// <param name="wrapper">MessageWrapper wrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationUpload(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var request = MessageDataHelper.FromMessageData<ITUtil.FTP.DTO.FTPUploadRequest>(wrapper.messageData);

            ITUtil.FTP.DTO.FTPSettings settings = FTP.DTO.FTPSettings.LoadSettings(request.ftpId);
            FtpWebRequest ftprequest = (FtpWebRequest)WebRequest.Create(settings.serverUri);
            ftprequest.Credentials = new NetworkCredential(settings.userName, settings.password);
            ftprequest.Method = WebRequestMethods.Ftp.UploadFile;
            ftprequest.UsePassive = settings.usePassive;
            ftprequest.EnableSsl = settings.enableSsl;

            foreach (var file in request.files)
            {
                ftprequest.ContentLength = file.fileContent.ToArray().Length;

                using (Stream requestStream = ftprequest.GetRequestStream())
                {
                    requestStream.Write(file.fileContent.ToArray(), 0, file.fileContent.ToArray().Length);
                }

                // using (FtpWebResponse response = (FtpWebResponse)ftprequest.GetResponse())
                // {
                //    onEvent("fileuploaded", new FTPOpResult() { code = response.StatusCode, description = response.StatusDescription });
                // }
            }

            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }
    }
}
